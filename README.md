# More info:  
  
> [WIKI](https://bitbucket.org/kamaradski/t-a/wiki/Home)    
> [F.A.Q](https://bitbucket.org/kamaradski/t-a/wiki/Frequently%20Asked%20Questions)  
> [Change-Log](https://bitbucket.org/kamaradski/t-a/wiki/Change-log)  
> [Future ideas](https://bitbucket.org/kamaradski/t-a/issues?q=TODO&status=new&status=open)  
  
# Forums & releases:  
  
> [AhoyWorld Forums](http://www.ahoyworld.co.uk/topic/2709-transport-ambush-the-thread)  
> [BIS Forums](http://forums.bistudio.com/showthread.php?184558-Transport-amp-Ambush-(Altis))  
> [Armaholic](http://www.armaholic.com/page.php?id=27087)  
> [Steam Workshop](http://steamcommunity.com/sharedfiles/filedetails/?id=275140474)  
  