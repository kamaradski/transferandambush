//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			vehInitFunctins.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// ServerSide Vehicle INIT functions definition for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

kINTFuelLoad=(round(random 10))/10;

K_FNC_Hunfrit = {
	kINTFuelLoad=(round(random 10))/10;
	_veh = _this select 0;
	_veh setFuel kINTFuelLoad;
	_veh disableTIEquipment true;
	[[[_veh],"int_scripts\clearinv.sqf"],"BIS_fnc_execVM",nil,true] spawn BIS_fnc_MP;
	diag_log format ["%1: Transport & Ambush: Vehicle INIT was run for: %2", time, _veh];
};

K_FNC_off = {
	kINTFuelLoad=(round(random 10))/10;
	_veh = _this select 0;
	_veh setFuel kINTFuelLoad;
	[[[_veh],"int_scripts\clearinv.sqf"],"BIS_fnc_execVM",nil,true] spawn BIS_fnc_MP;
	diag_log format ["%1: Transport & Ambush: Vehicle INIT was run for: %2", time, _veh];
};

K_FNC_helo = {
	kINTFuelLoad=(round(random 10))/10;
	_veh = _this select 0;
	_veh setFuel kINTFuelLoad;
	[[[_veh],"int_scripts\clearinv.sqf"],"BIS_fnc_execVM",nil,true] spawn BIS_fnc_MP;
	diag_log format ["%1: Transport & Ambush: Vehicle INIT was run for: %2", time, _veh];
};

K_FNC_armor = {
	kINTFuelLoad=(round(random 10))/10;
	_veh = _this select 0;
	_veh setFuel kINTFuelLoad;
	diag_log format ["%1: Transport & Ambush: Vehicle INIT was run for: %2", time, _veh];
};
