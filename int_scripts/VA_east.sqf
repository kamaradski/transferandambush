//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			VA_east.sqf
// Author:			Kamaradski 2014
// Contributers:	Razgriz33
//
// 
// Drop-in Virtual Arsenal ammo-box for "Transport & Ambush" & I&A
// _K = [this] execVM "int_scripts\VA_east.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Setup VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_myBox = _this select 0;

_myBox allowDamage false; 
_myBox enableSimulation false;

clearWeaponCargo _myBox;
clearMagazineCargo _myBox;
clearBackpackCargo _myBox;
clearItemCargo _myBox;

["AmmoboxInit",[_myBox,true]] call BIS_fnc_arsenal;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Empty VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[true],true] call BIS_fnc_removeVirtualBackpackCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualItemCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualWeaponCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualMagazineCargo;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add side independent gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"V_Chestrig_oli",
	"H_Shemag_olive",
	"ItemMap",
	"ItemCompass",
	"ItemWatch",
	"ItemRadio",
	"FirstAidKit",
	"V_BandollierB_blk",
	"H_Cap_oli",
	"V_Chestrig_blk",
	"H_Watchcap_blk",
	"V_TacVest_blk",
	"H_Booniehat_khk",
	"G_Goggles_VR",
	"H_Bandanna_khk",
	"H_Watchcap_camo",
	"V_BandollierB_khk",
	"ItemGPS"
],true] call BIS_fnc_addVirtualItemCargo;	

[_myBox,[
	"LMG_M200",
	"missiles_titan",
	"SmokeLauncher",
	"autocannon_35mm",
	"mortar_155mm_AMOS",
	"GMG_40mm",
	"HMG_127_APC",
	"Laserdesignator_mounted",
	"GBU12BombLauncher",
	"TruckHorn",
	"HMG_127",
	"SportCarHorn",
	"HMG_M2",
	"MiniCarHorn",
	"CarHorn",
	"missiles_titan_static",
	"GMG_UGV_40mm",
	"missiles_SCALPEL",
	"missiles_DAGR",
	"CMFlareLauncher",
	"HMG_01",
	"GMG_20mm",
	"mortar_82mm",
	"Rangefinder",
	"Laserdesignator",
	"arifle_SDAR_F",
	"arifle_TRG21_F",
	"Throw",
	"Put",
	"arifle_TRG20_F",
	"arifle_TRG20_ACO_F",
	"hgun_ACPC2_F",
	"Binocular",
	"arifle_Mk20_GL_ACO_F",
	"LMG_Mk200_F",
	"arifle_Mk20_F",
	"arifle_Mk20C_ACO_F",
	"arifle_TRG21_GL_F",
	"arifle_Mk20_MRCO_F",
	"launch_RPG32_F",
	"arifle_TRG21_MRCO_F"
],true] call BIS_fnc_addVirtualWeaponCargo;	
	
	
[_myBox,[
	"30Rnd_556x45_Stanag",
	"Chemlight_blue",
	"30Rnd_556x45_Stanag",
	"30Rnd_556x45_Stanag_Tracer_Yellow",
	"9Rnd_45ACP_Mag",
	"SmokeShellRed",
	"SmokeShellBlue",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_Smoke_Grenade_shell",
	"1Rnd_SmokeGreen_Grenade_shell",
	"1Rnd_SmokeRed_Grenade_shell",
	"1Rnd_SmokeBlue_Grenade_shell",
	"200Rnd_65x39_cased_Box",
	"APERSMine_Range_Mag",
	"HandGrenade",
	"MiniGrenade",
	"30Rnd_556x45_Stanag",
	"RPG32_F",
	"30Rnd_556x45_Stanag",
	"16Rnd_9x21_Mag",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_SmokeOrange_Grenade_shell",
	"SmokeShellOrange",
	"30Rnd_556x45_Stanag",
	"20Rnd_556x45_UW_mag",
	"SmokeShellRed",
	"ClaymoreDirectionalMine_Remote_Mag",
	"APERSTripMine_Wire_Mag",
	"Laserbatteries",
	"6Rnd_LG_scalpel",
	"2Rnd_GBU12_LGB",
	"100Rnd_127x99_mag_Tracer_Yellow"
],true] call BIS_fnc_addVirtualMagazineCargo;



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add OPFOR gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"O_HMG_01_weapon_F",
	"O_GMG_01_weapon_F",
	"O_HMG_01_high_weapon_F",
	"O_GMG_01_high_weapon_F",
	"O_Mortar_01_weapon_F",
	"B_AssaultPack_rgr",
	"B_BergenC_grn",
	"B_Carryall_khk",
	"B_FieldPack_khk",
	"B_Kitbag_rgr",
	"B_OutdoorPack_blu",
	"B_TacticalPack_rgr",
	"B_Parachute"
],true] call BIS_fnc_addVirtualBackpackCargo;
	

[_myBox,[
	"U_OG_Guerilla1_1",
	"U_OG_leader",
	"U_OG_Guerilla2_1",
	"U_OG_Guerilla2_3",
	"U_OG_Guerilla2_2",
	"U_OG_Guerilla3_1",
	"U_OG_Guerrilla_6_1",
	"U_O_OfficerUniform_ocamo",
	"H_Beret_ocamo",
	"U_O_PilotCoveralls",
	"V_HarnessO_brn",
	"H_HelmetO_ocamo",
	"NVGoggles_OPFOR",
	"U_O_CombatUniform_ocamo",
	"H_MilCap_ocamo",
	"V_HarnessOGL_brn",
	"H_HelmetLeaderO_ocamo",
	"V_TacVest_khk",
	"H_PilotHelmetHeli_O",
	"U_O_SpecopsUniform_ocamo",
	"H_HelmetCrew_O",
	"H_PilotHelmetFighter_O",
	"H_CrewHelmetHeli_O",
	"V_TacVest_brn",
	"O_UavTerminal",
	"U_O_Wetsuit",
	"V_RebreatherIR",
	"G_O_Diving",
	"U_O_GhillieSuit",
	"V_Chestrig_khk",
	"V_HarnessOSpec_brn",
	"H_HelmetSpecO_ocamo",
	"H_HelmetSpecO_blk",
	"U_O_CombatUniform_oucamo",
	"V_HarnessO_gry",
	"H_HelmetO_oucamo",
	"V_HarnessOGL_gry",
	"H_HelmetLeaderO_oucamo",
	"U_O_Protagonist_VR"
],true] call BIS_fnc_addVirtualItemCargo;


[_myBox,[
	"arifle_Katiba_C_ACO_F",
	"hgun_Pistol_heavy_02_Yorris_F",
	"arifle_Katiba_ACO_pointer_F",
	"hgun_Rook40_F",
	"arifle_Katiba_ACO_F",
	"arifle_Katiba_GL_ACO_F",
	"LMG_Zafir_pointer_F",
	"arifle_Katiba_ARCO_pointer_F",
	"arifle_Katiba_GL_ARCO_pointer_F",
	"srifle_DMR_01_DMS_F",
	"arifle_Katiba_pointer_F",
	"arifle_Katiba_C_ACO_pointer_F",
	"SMG_02_ACO_F",
	"launch_O_Titan_short_F",
	"launch_O_Titan_F",
	"arifle_Katiba_C_F",
	"hgun_Rook40_snds_F",
	"arifle_Katiba_ARCO_F",
	"srifle_GM6_camo_LRPS_F",
	"arifle_Katiba_ACO_pointer_snds_F",
	"srifle_DMR_01_DMS_snds_F",
	"arifle_Katiba_C_ACO_pointer_snds_F",
	"arifle_Katiba_GL_ACO_pointer_snds_F",
	"arifle_Katiba_ARCO_pointer_snds_F"
],true] call BIS_fnc_addVirtualWeaponCargo;


[_myBox,[
	"30Rnd_65x39_caseless_green",
	"6Rnd_45ACP_Cylinder",
	"Chemlight_red",
	"30Rnd_65x39_caseless_green",
	"30Rnd_65x39_caseless_green",
	"1Rnd_SmokeYellow_Grenade_shell",
	"150Rnd_762x51_Box",
	"30Rnd_65x39_caseless_green_mag_Tracer",
	"O_IR_Grenade",
	"SmokeShellYellow",
	"10Rnd_762x51_Mag",
	"30Rnd_9x21_Mag",
	"30Rnd_65x39_caseless_green",
	"Chemlight_red",
	"Chemlight_red",
	"5Rnd_127x108_Mag",
	"5Rnd_127x108_APDS_Mag",
	"30Rnd_65x39_caseless_green"
],true] call BIS_fnc_addVirtualMagazineCargo;



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Remove globally restricted gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-	

[_myBox,[
	"hgun_PDW2000_F",
	"arifle_Mk20C_F",
	"arifle_Mk20_GL_F",
	"arifle_Mk20_plain_F",
	"arifle_Mk20C_plain_F",
	"arifle_Mk20_GL_plain_F",
	"srifle_EBR_F",
	"srifle_GM6_F",
	"launch_I_Titan_F",
	"launch_I_Titan_short_F",
	"launch_Titan_F",
	"launch_Titan_short_F"
],true] call BIS_fnc_removeVirtualWeaponCargo;	
	
	
[_myBox,[
	"B_HMG_01_support_F",
	"B_HMG_01_support_high_F",
	"B_Mortar_01_support_F",
	"I_HMG_01_weapon_F",
	"I_GMG_01_weapon_F",
	"I_HMG_01_high_weapon_F",
	"I_GMG_01_high_weapon_F",
	"I_HMG_01_support_F",
	"I_HMG_01_support_high_F",
	"I_Mortar_01_weapon_F",
	"I_Mortar_01_support_F",
	"O_HMG_01_support_F",
	"O_GMG_01_support_high_F",
	"O_Mortar_01_support_F",
	"B_UAV_01_backpack_F",
	"O_UAV_01_backpack_F",
	"I_UAV_01_backpack_F"
],true] call BIS_fnc_removeVirtualBackpackCargo;


[_myBox,[
	"NLAW_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA",
	"RPG32_F",
	"RPG32_HE_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA"
],true] call BIS_fnc_removeVirtualMagazineCargo;
	

[_myBox,[
	"optic_NVS",
	"optic_Nightstalker",
	"optic_tws",
	"optic_tws_mg",
	"U_I_CombatUniform",
	"U_I_CombatUniform_shortsleeve",
	"U_I_CombatUniform_tshirt",
	"U_I_OfficerUniform",
	"U_I_GhillieSuit",
	"U_I_HeliPilotCoveralls",
	"U_I_pilotCoveralls",
	"U_I_Wetsuit",
	"U_IG_Guerilla1_1",
	"U_IG_Guerilla2_1",
	"U_IG_Guerilla2_2",
	"U_IG_Guerilla2_3",
	"U_IG_Guerilla3_1",
	"U_IG_Guerilla3_2",
	"U_IG_leader",
	"U_BG_Guerilla3_2",
	"U_OG_Guerilla3_2",
	"U_C_Poloshirt_blue",
	"U_C_Poloshirt_burgundy",
	"U_C_Poloshirt_stripped",
	"U_C_Poloshirt_tricolour",
	"U_C_Poloshirt_salmon",
	"U_C_Poloshirt_redwhite",
	"U_C_Commoner1_1",
	"U_C_Commoner1_2",
	"U_C_Commoner1_3",
	"U_C_Poor_1",
	"U_C_Poor_2",
	"U_C_Scavenger_1",
	"U_C_Scavenger_2",
	"U_C_Farmer",
	"U_C_Fisherman",
	"U_C_WorkerOveralls",
	"U_C_FishermanOveralls",
	"U_C_WorkerCoveralls",
	"U_C_HunterBody_grn",
	"U_C_HunterBody_brn",
	"U_C_Commoner2_1",
	"U_C_Commoner2_2",
	"U_C_Commoner2_3",
	"U_C_PriestBody",
	"U_C_Poor_shorts_1",
	"U_C_Poor_shorts_2",
	"U_C_Commoner_shorts",
	"U_C_ShirtSurfer_shorts",
	"U_C_TeeSurfer_shorts_1",
	"U_C_TeeSurfer_shorts_2",
	"U_NikosBody",
	"U_MillerBody",
	"U_KerryBody",
	"U_OrestesBody",
	"U_AttisBody",
	"U_AntigonaBody",
	"U_IG_Menelaos",
	"U_C_Novak",
	"U_OI_Scientist",
	"V_PlateCarrierIA1_dgtl",
	"V_PlateCarrierIA2_dgtl",
	"V_PlateCarrierIAGL_dgtl",
	"V_RebreatherIA",
	"V_TacVest_oli",
	"V_TacVest_camo",
	"V_TacVest_blk_POLICE",
	"V_TacVestIR_blk",
	"V_TacVestCamo_khk",
	"V_BandollierB_cbr",
	"V_BandollierB_oli",
	"H_HelmetIA",
	"H_HelmetIA_net",
	"H_HelmetIA_camo",
	"H_HelmetCrew_I",
	"H_CrewHelmetHeli_I",
	"H_PilotHelmetHeli_I",
	"H_PilotHelmetFighter_I",
	"H_Booniehat_dgtl",
	"H_Booniehat_indp",
	"H_MilCap_dgtl",
	"H_Cap_grn",
	"H_Cap_red",
	"H_Cap_blu",
	"H_Cap_tan",
	"H_Cap_blk",
	"H_Cap_grn_BI",
	"H_Cap_blk_Raven",
	"H_Cap_blk_ION",
	"H_Cap_blk_CMMG",
	"H_MilCap_rucamo",
	"H_MilCap_gry",
	"H_MilCap_blue",
	"H_Booniehat_grn",
	"H_Booniehat_tan",
	"H_Booniehat_dirty",
	"H_StrawHat",
	"H_StrawHat_dark",
	"H_Hat_blue",
	"H_Hat_brown",
	"H_Hat_camo",
	"H_Hat_grey",
	"H_Hat_checker",
	"H_Hat_tan",
	"H_Bandanna_surfer",
	"H_Bandanna_cbr",
	"H_Bandanna_sgg",
	"H_Bandanna_gry",
	"H_Bandanna_camo",
	"H_TurbanO_blk",
	"H_Shemag_khk",
	"H_Shemag_tan",
	"H_ShemagOpen_khk",
	"H_ShemagOpen_tan",
	"H_Beret_blk",
	"H_Beret_blk_POLICE",
	"H_Beret_red",
	"H_Beret_grn",
	"H_Watchcap_khk",
	"H_Watchcap_sgg",
	"H_BandMask_blk",
	"H_BandMask_khk",
	"H_BandMask_reaper",
	"H_BandMask_demon",
	"H_Shemag_olive_hs",
	"H_Cap_oli_hs",
	"I_UavTerminal",
	"H_Booniehat_khk_hs"
],true] call BIS_fnc_removeVirtualItemCargo;

diag_log format ["%1: Transport & Ambush: EAST Virtual Arsenal initiated for: %2:", time, _myBox];