//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			kAIspawn.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// Server Side AI spawn and waypoint script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


if (!isServer) exitWith {};  // server only script
sleep 5;

_kARRmarkers = ["AI1","AI2","AI3","AI4","AI5","AI6","AI7","AI8","AI9","AI10","AI11","AI12","AI13","AI14","AI15","AI16","AI17","AI18","AI19","AI20","AI21","AI22","AI23","AI24","AI25"];


// Create spawn locations
	_kspawngrp1 = _kARRmarkers call BIS_fnc_selectRandom;
	_kspawngrp2 = _kARRmarkers call BIS_fnc_selectRandom;
	_kspawngrp3 = _kARRmarkers call BIS_fnc_selectRandom;
	_kspawngrp4 = _kARRmarkers call BIS_fnc_selectRandom;

	_kspawngrp5 = _kARRmarkers call BIS_fnc_selectRandom;
	_kspawngrp6 = _kARRmarkers call BIS_fnc_selectRandom;
	_kspawngrp7 = _kARRmarkers call BIS_fnc_selectRandom;
	_kspawngrp8 = _kARRmarkers call BIS_fnc_selectRandom;
	

// Create waypoint1 array
	_kARRwp1grp1 = _kARRmarkers - [ _kspawngrp1 ];
	_kARRwp1grp2 = _kARRmarkers - [ _kspawngrp2 ];
	_kARRwp1grp3 = _kARRmarkers - [ _kspawngrp3 ];
	_kARRwp1grp4 = _kARRmarkers - [ _kspawngrp4 ];
	
	_kARRwp1grp5 = _kARRmarkers - [ _kspawngrp5 ];
	_kARRwp1grp6 = _kARRmarkers - [ _kspawngrp6 ];
	_kARRwp1grp7 = _kARRmarkers - [ _kspawngrp7 ];
	_kARRwp1grp8 = _kARRmarkers - [ _kspawngrp8 ];
	

// Create waypoint2 array
	_kARRwp2grp1 = _kARRmarkers - [ _kspawngrp1, _kARRwp1grp1 ];
	_kARRwp2grp2 = _kARRmarkers - [ _kspawngrp2, _kARRwp1grp2 ];
	_kARRwp2grp3 = _kARRmarkers - [ _kspawngrp3, _kARRwp1grp3 ];
	_kARRwp2grp4 = _kARRmarkers - [ _kspawngrp4, _kARRwp1grp4 ];
	
	_kARRwp2grp5 = _kARRmarkers - [ _kspawngrp5, _kARRwp1grp5 ];
	_kARRwp2grp6 = _kARRmarkers - [ _kspawngrp6, _kARRwp1grp6 ];
	_kARRwp2grp7 = _kARRmarkers - [ _kspawngrp7, _kARRwp1grp7 ];
	_kARRwp2grp8 = _kARRmarkers - [ _kspawngrp8, _kARRwp1grp8 ];
	

// Select waypoint1
	_kwp1grp1 = _kARRwp1grp1 call BIS_fnc_selectRandom;
	_kwp1grp2 = _kARRwp1grp2 call BIS_fnc_selectRandom;
	_kwp1grp3 = _kARRwp1grp3 call BIS_fnc_selectRandom;
	_kwp1grp4 = _kARRwp1grp4 call BIS_fnc_selectRandom;
	
	_kwp1grp5 = _kARRwp1grp5 call BIS_fnc_selectRandom;
	_kwp1grp6 = _kARRwp1grp6 call BIS_fnc_selectRandom;
	_kwp1grp7 = _kARRwp1grp7 call BIS_fnc_selectRandom;
	_kwp1grp8 = _kARRwp1grp8 call BIS_fnc_selectRandom;
	

// Select waypoint2
	_kwp2grp1 = _kARRwp2grp1 call BIS_fnc_selectRandom;
	_kwp2grp2 = _kARRwp2grp2 call BIS_fnc_selectRandom;
	_kwp2grp3 = _kARRwp2grp3 call BIS_fnc_selectRandom;
	_kwp2grp4 = _kARRwp2grp4 call BIS_fnc_selectRandom;
	
	_kwp2grp5 = _kARRwp2grp5 call BIS_fnc_selectRandom;
	_kwp2grp6 = _kARRwp2grp6 call BIS_fnc_selectRandom;
	_kwp2grp7 = _kARRwp2grp7 call BIS_fnc_selectRandom;
	_kwp2grp8 = _kARRwp2grp8 call BIS_fnc_selectRandom;


// Select sides
	_side1 = west;
	_side2 = east;

sleep 5;
	
	
// Spawn group-1 (west - AA)
	_grp1 = createGroup _side1; 
	_grp1 = [ getMarkerPos _kspawngrp1, _side1, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >>  "BUS_InfTeam_AA")] call BIS_fnc_spawnGroup; 
	
	_gr1wp0 = _grp1 addWaypoint [ getMarkerPos _kwp1grp1, 0 ]; 
	_gr1wp0 setWaypointType "LOITER"; 
	_gr1wp0 setWaypointFormation "COLUMN"; 
	_gr1wp0 setWaypointBehaviour "AWARE"; 
	_gr1wp0 setwaypointcombatmode "RED"; 
	_gr1wp0 setWaypointSpeed "NORMAL"; 
	_gr1wp0 setWaypointCompletionRadius 20;  
	_gr1wp1 = _grp1 addWaypoint [ getMarkerPos _kwp2grp1, 0 ]; 
	_gr1wp2 = _grp1 addWaypoint [ getMarkerPos _kspawngrp1, 0 ]; 
	_gr1wp2 setWaypointType "CYCLE"; 
	sleep 0.5;

	
// Spawn group-2 (west - squad)
	_grp2 = createGroup _side1; 
	_grp2 = [ getMarkerPos _kspawngrp2, _side1, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >>  "BUS_InfSquad")] call BIS_fnc_spawnGroup; 
	
	_gr2wp0 = _grp2 addWaypoint [ getMarkerPos _kwp1grp2, 0 ]; 
	_gr2wp0 setWaypointType "LOITER"; 
	_gr2wp0 setWaypointFormation "COLUMN"; 
	_gr2wp0 setWaypointBehaviour "AWARE"; 
	_gr2wp0 setwaypointcombatmode "RED"; 
	_gr2wp0 setWaypointSpeed "NORMAL"; 
	_gr2wp0 setWaypointCompletionRadius 20;  
	_gr2wp1 = _grp2 addWaypoint [ getMarkerPos _kwp2grp2, 0 ]; 
	_gr2wp2 = _grp2 addWaypoint [ getMarkerPos _kspawngrp2, 0 ]; 
	_gr2wp2 setWaypointType "CYCLE"; 
	sleep 0.5;

	
// Spawn group-3 (west - Helo)
	_grp3 = createGroup _side1; 
	_grp3 = [ getMarkerPos _kspawngrp3, 316, "B_Heli_Light_01_armed_F", _side1 ] call BIS_fnc_spawnVehicle;
	_grp3 = _grp3 select 2;
	
	_gr3wp0 = _grp3 addWaypoint [ getMarkerPos _kwp1grp3, 0 ]; 
	_gr3wp0 setWaypointType "SAD"; 
	_gr3wp0 setWaypointFormation "COLUMN"; 
	_gr3wp0 setWaypointBehaviour "AWARE"; 
	_gr3wp0 setwaypointcombatmode "RED"; 
	_gr3wp0 setWaypointSpeed "LIMITED"; 
	_gr3wp0 setWaypointCompletionRadius 20;  
	_gr3wp1 = _grp3 addWaypoint [ getMarkerPos _kwp2grp3, 0 ]; 
	_gr3wp2 = _grp3 addWaypoint [ getMarkerPos _kspawngrp3, 0 ]; 
	_gr3wp2 setWaypointType "CYCLE"; 
	sleep 0.5;
	
	
// Spawn group-4 (west - Vehicle)
	_grp4 = createGroup _side1; 
	_grp4 = [ getMarkerPos _kspawngrp4, 316, "B_APC_Tracked_01_rcws_F", _side1 ] call BIS_fnc_spawnVehicle;
	_grp4 = _grp4 select 2;
	
	_gr4wp0 = _grp4 addWaypoint [ getMarkerPos _kwp1grp4, 0 ]; 
	_gr4wp0 setWaypointType "SAD"; 
	_gr4wp0 setWaypointFormation "COLUMN"; 
	_gr4wp0 setWaypointBehaviour "AWARE"; 
	_gr4wp0 setwaypointcombatmode "RED"; 
	_gr4wp0 setWaypointSpeed "LIMITED"; 
	_gr4wp0 setWaypointCompletionRadius 20;  
	_gr4wp1 = _grp4 addWaypoint [ getMarkerPos _kwp2grp4, 0 ]; 
	_gr4wp2 = _grp4 addWaypoint [ getMarkerPos _kspawngrp4, 0 ]; 
	_gr4wp2 setWaypointType "CYCLE"; 

diag_log format ["%1: Transport & Ambush: Server spawned WEST random patrols:", time];
	
sleep 5;
	
// Spawn group-5 (east - AA)
	_grp5 = createGroup _side2; 
	_grp5 = [ getMarkerPos _kspawngrp5, _side2, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >>  "OIA_InfTeam_AA")] call BIS_fnc_spawnGroup; 
	
	_gr5wp0 = _grp5 addWaypoint [ getMarkerPos _kwp1grp5, 0 ]; 
	_gr5wp0 setWaypointType "LOITER"; 
	_gr5wp0 setWaypointFormation "COLUMN"; 
	_gr5wp0 setWaypointBehaviour "AWARE"; 
	_gr5wp0 setwaypointcombatmode "RED"; 
	_gr5wp0 setWaypointSpeed "NORMAL"; 
	_gr5wp0 setWaypointCompletionRadius 20;  
	_gr5wp1 = _grp5 addWaypoint [ getMarkerPos _kwp2grp5, 0 ]; 
	_gr5wp2 = _grp5 addWaypoint [ getMarkerPos _kspawngrp5, 0 ]; 
	_gr5wp2 setWaypointType "CYCLE";
	sleep 0.5;	

	
// Spawn group-6 (east - squad)
	_grp6 = createGroup _side2; 
	_grp6 = [ getMarkerPos _kspawngrp6, _side2, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >>  "OIA_InfSquad")] call BIS_fnc_spawnGroup; 
	
	_gr6wp0 = _grp6 addWaypoint [ getMarkerPos _kwp1grp6, 0 ]; 
	_gr6wp0 setWaypointType "LOITER"; 
	_gr6wp0 setWaypointFormation "COLUMN"; 
	_gr6wp0 setWaypointBehaviour "AWARE"; 
	_gr6wp0 setwaypointcombatmode "RED"; 
	_gr6wp0 setWaypointSpeed "NORMAL"; 
	_gr6wp0 setWaypointCompletionRadius 20;  
	_gr6wp1 = _grp6 addWaypoint [ getMarkerPos _kwp2grp6, 0 ]; 
	_gr6wp2 = _grp6 addWaypoint [ getMarkerPos _kspawngrp6, 0 ]; 
	_gr6wp2 setWaypointType "CYCLE"; 
	sleep 0.5;

	
// Spawn group-7 (east - Helo)
	_grp7 = createGroup _side2; 
	_grp7 = [ getMarkerPos _kspawngrp7, 316, "O_Heli_Light_02_F", _side2 ] call BIS_fnc_spawnVehicle;
	
	_grp7 = _grp7 select 2;
	_gr7wp0 = _grp7 addWaypoint [ getMarkerPos _kwp1grp7, 0 ]; 
	_gr7wp0 setWaypointType "SAD"; 
	_gr7wp0 setWaypointFormation "COLUMN"; 
	_gr7wp0 setWaypointBehaviour "AWARE"; 
	_gr7wp0 setwaypointcombatmode "RED"; 
	_gr7wp0 setWaypointSpeed "LIMITED"; 
	_gr7wp0 setWaypointCompletionRadius 20;  
	_gr7wp1 = _grp7 addWaypoint [ getMarkerPos _kwp2grp7, 0 ]; 
	_gr7wp2 = _grp7 addWaypoint [ getMarkerPos _kspawngrp7, 0 ]; 
	_gr7wp2 setWaypointType "CYCLE"; 
	sleep 0.5;
	
	
// Spawn group-8 (east - Vehicle)
	_grp8 = createGroup _side2; 
	_grp8 = [ getMarkerPos _kspawngrp8, 316, "O_APC_Tracked_02_cannon_F", _side2 ] call BIS_fnc_spawnVehicle;
	_grp8 = _grp8 select 2;
	
	_gr8wp0 = _grp8 addWaypoint [ getMarkerPos _kwp1grp8, 0 ]; 
	_gr8wp0 setWaypointType "SAD"; 
	_gr8wp0 setWaypointFormation "COLUMN"; 
	_gr8wp0 setWaypointBehaviour "AWARE"; 
	_gr8wp0 setwaypointcombatmode "RED"; 
	_gr8wp0 setWaypointSpeed "LIMITED"; 
	_gr8wp0 setWaypointCompletionRadius 20;  
	_gr8wp1 = _grp8 addWaypoint [ getMarkerPos _kwp2grp8, 0 ]; 
	_gr8wp2 = _grp8 addWaypoint [ getMarkerPos _kspawngrp8, 0 ]; 
	_gr8wp2 setWaypointType "CYCLE"; 
	
diag_log format ["%1: Transport & Ambush: Server spawned EAST random patrols:", time];