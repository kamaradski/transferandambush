// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			kmissionEnd.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// Client-side mission ending script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

waitUntil {sleep 2; kINTsomewin==1;};
		
if (kINTfblu!=0) then {								// if BLUE wins
	if (playerSide == west) then {
		diag_log format ["%1: Transport & Ambush: BLUE has won:", time];
		hint "Blue Wins";
		["END1",true,5] call BIS_fnc_endMission;
	};
	if (playerside == east) then {
		diag_log format ["%1: Transport & Ambush: RED has won:", time];
		hint "Red LOST";
		["END1",false,5] call BIS_fnc_endMission;
	};
};

if (kINTfred==1) then {								// if RED wins
	if (playerside == west) then {
		hint "Blue LOST";
		["END1",false,5] call BIS_fnc_endMission;
	};
	if (playerside == east) then {
		hint "Red Wins";
		["END1",true,5] call BIS_fnc_endMission;
	};
};
