// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			kmissionRun.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// Server Side mission script for "Transport & Ambush"
//
 // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

diag_log format ["%1: Transport & Ambush: Server started main mission script", time];

if (isServer) then {
	
	kARRactivedestinations = ["desvikos","desathanos","descastle","desselakano","desgori"];
	
	kFUNC_vikos = {
	// RED task & trigger
		["Task2","Intercept convoy to Vikos","The NATO Troops are supplying Vikos!! Intercept these supplies, and bring them to our HQ!",EAST,["Kintercept",getMarkerPos"KVIKOS","hd_end","ColorRed"]] call SHK_Taskmaster_add;
		_kHANDtrg2 = createTrigger["EmptyDetector",getMarkerPos"unloadred"];
		_kHANDtrg2 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg2 setTriggerStatements ["this; kHANDkHANDwhat = getMarkerPos""unloadred"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfred=1;kINTsomewin=1;","kINTfred=0"];
		_kHANDtrg2 setTriggerText "Unload here";
		unloadredpad setpos (getMarkerPos"unloadred");
		unloadredpad setDir -89.30;
		sleep 30;
		
		// Blue task & trigger
		["Task1","Deliver to Vikos","The NATO Troops in Vikos are in desperate need of supplies !",WEST,["KDest",getMarkerPos"KVIKOS","hd_end","ColorBlue"]] call SHK_Taskmaster_add;   
		_kHANDtrg1=createTrigger["EmptyDetector",getMarkerPos"KVIKOS"];
		_kHANDtrg1 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg1 setTriggerStatements ["this; kHANDwhat = getMarkerPos""KVIKOS"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfblu=1;kINTsomewin=1;","kINTfblu=0"];
		_kHANDtrg1 setTriggerText "Unload here";
		KamDest setpos (getMarkerPos"KVIKOS");
		KamDest setDir 89.30;
		sleep 1;

		// wait & score
		diag_log format ["%1: Transport & Ambush: Mission successfully started, waiting for outcome:", time];
		null = [kINTrandom] execVM "int_scripts\kdestprotect.sqf";
		waitUntil {sleep 1; kINTsomewin==1;};
		publicVariable "kINTsomewin";
		if (kINTfblu==1) then { 
			publicVariable "kINTfblu"; 
			["Task1","succeeded"] call SHK_Taskmaster_upd; 
			["Task2","failed"] call SHK_Taskmaster_upd; 
		};
		if (kINTfred==1) then {
			publicVariable "kINTfred";
			["Task1","failed"] call SHK_Taskmaster_upd; 
			["Task2","succeeded"] call SHK_Taskmaster_upd; 
		};
		deleteVehicle _kHANDtrg1;
		deleteVehicle _kHANDtrg2;
	};

kFUNC_athanos = {

	// RED task & trigger
		["Task4","Intercept convoy to Athanos","The NATO Troops are supplying Athanos!! Intercept these supplies, and bring them to our HQ!",EAST,["Kintercept",getMarkerPos"kAthanos","hd_end","ColorRed"]] call SHK_Taskmaster_add;
		_kHANDtrg2=createTrigger["EmptyDetector",getMarkerPos"unloadred"];
		_kHANDtrg2 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg2 setTriggerStatements ["this; kHANDwhat = getMarkerPos""unloadred"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfred=1;kINTsomewin=1;","kINTfred=0"];
		_kHANDtrg2 setTriggerText "Unload here";
		unloadredpad setpos (getMarkerPos"unloadred");
		unloadredpad setDir -89.30;
		sleep 30;
	
// Blue task & trigger
		["Task3","Deliver to Athanos","The NATO Troops in Athanos are in desperate need of supplies !",WEST,["KDest",getMarkerPos"kAthanos","hd_end","ColorBlue"]] call SHK_Taskmaster_add;   
		_kHANDtrg1=createTrigger["EmptyDetector",getMarkerPos"kAthanos"];
		_kHANDtrg1 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg1 setTriggerStatements ["this; kHANDwhat = getMarkerPos""kAthanos"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfblu=1;kINTsomewin=1;","kINTfblu=0"];
		_kHANDtrg1 setTriggerText "Unload here";
		KamDest setpos (getMarkerPos"kAthanos");
		KamDest setDir 89.30;
		sleep 1;
	
// wait & score
		diag_log format ["%1: Transport & Ambush: Mission successfully started, waiting for outcome:", time];
		null = [kINTrandom] execVM "int_scripts\kdestprotect.sqf";
		waitUntil { sleep 1; kINTsomewin==1;};
		publicVariable "kINTsomewin";
		if (kINTfblu==1) then {
			publicVariable "kINTfblu"; 
			["Task3","succeeded"] call SHK_Taskmaster_upd; 
			["Task4","failed"] call SHK_Taskmaster_upd; 
		};
		if (kINTfred==1) then {
			publicVariable "kINTfred";
			["Task3","failed"] call SHK_Taskmaster_upd; 
			["Task4","succeeded"] call SHK_Taskmaster_upd; 
		};
		deleteVehicle _kHANDtrg1;
		deleteVehicle _kHANDtrg2;
	};

	kFUNC_castle = {

	// RED task & trigger
		["Task6","Intercept convoy to the castle","The NATO Troops are supplying the castle!! Intercept these supplies, and bring them to our HQ!",EAST,["Kintercept",getMarkerPos"kCASTLE","hd_end","ColorRed"]] call SHK_Taskmaster_add;
		_kHANDtrg2=createTrigger["EmptyDetector",getMarkerPos"unloadred"];
		_kHANDtrg2 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg2 setTriggerStatements ["this; kHANDwhat = getMarkerPos""unloadred"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfred=1;kINTsomewin=1;","kINTfred=0"];
		_kHANDtrg2 setTriggerText "Unload here";
		unloadredpad setpos (getMarkerPos"unloadred");
		unloadredpad setDir -89.30;
		sleep 30;		
		
// Blue task & trigger
		["Task5","Deliver to the castle","The NATO Troops in the castle are in desperate need of supplies !",WEST,["KDest",getMarkerPos"kCASTLE","hd_end","ColorBlue"]] call SHK_Taskmaster_add;   
		_kHANDtrg1=createTrigger["EmptyDetector",getMarkerPos"kCASTLE"];
		_kHANDtrg1 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg1 setTriggerStatements ["this; kHANDwhat = getMarkerPos""kCASTLE"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfblu=1;kINTsomewin=1;","kINTfblu=0"];
		_kHANDtrg1 setTriggerText "Unload here";
		KamDest setpos (getMarkerPos"kCASTLE");
		KamDest setDir 23.34;
		sleep 1;
	
// wait & score
		diag_log format ["%1: Transport & Ambush: Mission successfully started, waiting for outcome:", time];
		null = [kINTrandom] execVM "int_scripts\kdestprotect.sqf";
		waitUntil {sleep 1; kINTsomewin==1;};
		publicVariable "kINTsomewin";
		if (kINTfblu==1) then {
			publicVariable "kINTfblu"; 
			["Task5","succeeded"] call SHK_Taskmaster_upd; 
			["Task6","failed"] call SHK_Taskmaster_upd; 
		};
		if (kINTfred==1) then {
			publicVariable "kINTfred";
			["Task5","failed"] call SHK_Taskmaster_upd; 
			["Task6","succeeded"] call SHK_Taskmaster_upd; 
		};
		deleteVehicle _kHANDtrg1;
		deleteVehicle _kHANDtrg2;
	};

	kFUNC_selakano = {

	// RED task & trigger
		["Task7","Intercept convoy to Selakano","The NATO Troops are supplying Selakano!! Intercept these supplies, and bring them to our HQ!",EAST,["Kintercept",getMarkerPos"kSELAKANO","hd_end","ColorRed"]] call SHK_Taskmaster_add;
		_kHANDtrg2=createTrigger["EmptyDetector",getMarkerPos"unloadred"];
		_kHANDtrg2 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg2 setTriggerStatements ["this; kHANDwhat = getMarkerPos""unloadred"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfred=1;kINTsomewin=1;","kINTfred=0"];
		_kHANDtrg2 setTriggerText "Unload here";
		unloadredpad setpos (getMarkerPos"unloadred");
		unloadredpad setDir -89.30;
		sleep 30;		
		
// Blue task & trigger
		["Task8","Deliver to Selakano","The NATO Troops in Selakano are in desperate need of supplies !",WEST,["KDest",getMarkerPos"kSELAKANO","hd_end","ColorBlue"]] call SHK_Taskmaster_add;   
		_kHANDtrg1=createTrigger["EmptyDetector",getMarkerPos"kSELAKANO"];
		_kHANDtrg1 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg1 setTriggerStatements ["this; kHANDwhat = getMarkerPos""kSELAKANO"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfblu=1;kINTsomewin=1;","kINTfblu=0"];
		_kHANDtrg1 setTriggerText "Unload here";
		KamDest setpos (getMarkerPos"kSELAKANO");
		KamDest setDir -314.68;
		sleep 1;
	
// wait & score
		diag_log format ["%1: Transport & Ambush: Mission successfully started, waiting for outcome:", time];
		null = [kINTrandom] execVM "int_scripts\kdestprotect.sqf";
		waitUntil { sleep 1; kINTsomewin==1;};
		publicVariable "kINTsomewin";
		if (kINTfblu==1) then {
			publicVariable "kINTfblu"; 
			["Task7","failed"] call SHK_Taskmaster_upd; 
			["Task8","succeeded"] call SHK_Taskmaster_upd; 
		};
		if (kINTfred==1) then {
			publicVariable "kINTfred";
			["Task7","succeeded"] call SHK_Taskmaster_upd; 
			["Task8","failed"] call SHK_Taskmaster_upd; 
		};
		deleteVehicle _kHANDtrg1;
		deleteVehicle _kHANDtrg2;
	};

	kFUNC_gori = {

	// RED task & trigger
		["Task9","Intercept convoy to Gori","The NATO Troops are supplying Gori!! Intercept these supplies, and bring them to our HQ!",EAST,["Kintercept",getMarkerPos"kGORI","hd_end","ColorRed"]] call SHK_Taskmaster_add;
		_kHANDtrg2=createTrigger["EmptyDetector",getMarkerPos"unloadred"];
		_kHANDtrg2 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg2 setTriggerStatements ["this; kHANDwhat = getMarkerPos""unloadred"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfred=1;kINTsomewin=1;","kINTfred=0"];
		_kHANDtrg2 setTriggerText "Unload here";
		unloadredpad setpos (getMarkerPos"unloadred");
		unloadredpad setDir -89.30;
		sleep 30;		
		
// Blue task & trigger
		["Task10","Deliver to Gori","The NATO Troops in Gori are in desperate need of supplies !",WEST,["KDest",getMarkerPos"kGORI","hd_end","ColorBlue"]] call SHK_Taskmaster_add;   
		_kHANDtrg1=createTrigger["EmptyDetector",getMarkerPos"kGORI"];
		_kHANDtrg1 setTriggerArea[10,10,0,FALSE];
		_kHANDtrg1 setTriggerStatements ["this; kHANDwhat = getMarkerPos""kGORI"" nearobjects [""Land_CargoBox_V1_F"",10];count kHANDwhat > 1","kINTfblu=1;kINTsomewin=1;","kINTfblu=0"];
		_kHANDtrg1 setTriggerText "Unload here";
		KamDest setpos (getMarkerPos"kGORI");
		KamDest setDir 274.91;
		sleep 1;
	
// wait & score
		diag_log format ["%1: Transport & Ambush: Mission successfully started, waiting for outcome:", time];
		null = [kINTrandom] execVM "int_scripts\kdestprotect.sqf";
		waitUntil {sleep 1; kINTsomewin==1;};
		publicVariable "kINTsomewin";
		if (kINTfblu==1) then {
			publicVariable "kINTfblu"; 
			["Task9","failed"] call SHK_Taskmaster_upd; 
			["Task10","succeeded"] call SHK_Taskmaster_upd; 
		};
		if (kINTfred==1) then {
			publicVariable "kINTfred";
			["Task9","succeeded"] call SHK_Taskmaster_upd; 
			["Task10","failed"] call SHK_Taskmaster_upd; 
		};
		deleteVehicle _kHANDtrg1;
		deleteVehicle _kHANDtrg2;
	};

		kINTrandom = kARRactivedestinations call BIS_fnc_selectRandom;
		diag_log format ["%1: Transport & Ambush: Server selected mission: %2", time, kINTrandom];
		if (kINTrandom == "desvikos") then { call kFUNC_vikos; };
		if (kINTrandom == "desathanos") then { call kFUNC_athanos; };
		if (kINTrandom == "descastle") then { call kFUNC_castle; };
		if (kINTrandom == "desselakano") then { call kFUNC_selakano; };
		if (kINTrandom == "desgori") then { call kFUNC_gori; };
		sleep 5;
};
